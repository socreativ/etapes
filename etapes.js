"use strict";

function fadeOut(container) {
    container.style.opacity = 0;
    setTimeout(() => {
        container.classList.remove('active');
    }, 500);
}

function fadeIn(container) {
    if (!container.classList.contains('active')) {
        container.classList.add('active');
        setTimeout(() => {
            container.style.opacity = 1;
        }, 500);
        
    }
}

docReady(() => {

    const sliderBlock = document.querySelectorAll('.etapes');

    sliderBlock.forEach(block => {

        const imageSliderElement = block.querySelector('.steps-img-slider');
        const triggers = block.querySelectorAll('.trigger');
        const contentSlide = block.querySelectorAll('.content-slide');
        let currentSlide = 0;
        let isAnimating = false;

        const imageSlider = new SoSlider(imageSliderElement, {
            fade: true,
            arrows: false,
            dots: false,
        });

        triggers.forEach((trigger, i) => trigger.addEventListener('click', () => {
            if(currentSlide !== i && !isAnimating) {
                isAnimating = true;
                imageSlider.slideGoTo(i);
                fadeOut(contentSlide[currentSlide]);
                triggers[currentSlide].classList.remove('active');
                currentSlide = i;
                trigger.classList.add('active');
                fadeIn(contentSlide[currentSlide]);
                setTimeout(() => isAnimating = false, 500);
            }
        }));

    });


});


