<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

wp_enqueue_script('SoSlider', 'https://cdn.jsdelivr.net/gh/ValMgr/SoSlider@1.5.1/SoSlider.js', array(), false, true);
wp_enqueue_style('soSlider', 'https://cdn.jsdelivr.net/gh/ValMgr/SoSlider@1.5.1/SoSlider.css', [], '1.0', 'all');

?>

<section id="<?= $id ?>" class="etapes <?= $css ?>">

    <div class="masked-image <?=  $maskClass ?>">
        <div class="greyscale"></div>
        <div class="steps-img-slider">
        <?php
            if(have_rows('elements')):
                while(have_rows('elements')): the_row();
                    $image = get_sub_field('image');
                    if($image): ?>
                        <?php if(my_wp_is_mobile()): ?>
                            <img class="etapes-image" src="<?=  $image['sizes']['large']; ?>"  alt="<?=  $image['alt']; ?>">
                        <?php else: ?>
                            <img class="etapes-image" src="<?=  $image['url']; ?>"  alt="<?=  $image['alt']; ?>">
                        <?php endif; ?>
                    <?php endif;
                endwhile;
            endif;

        ?>
        </div>
    </div>

    <div class="triggers d-flex">
        <?php if(have_rows('elements')): $i= 0;
            while(have_rows('elements')): $i++; the_row();
                $nbr = get_row_index(); ?>

                    <?php if($i!=1): ?>
                        <div class="trigger-spacer"></div>
                    <?php endif; ?>

                    <div class="trigger anim-300 <?php if($i==1) echo 'active'; ?>" trigger-id="<?=  $i-1 ?>"><p class="number-steps anim-300"><?=  $nbr; ?></p><span class="trigger-bar anim-300"></span></div>
            <?php endwhile;
        endif; ?>
    </div>

    <div class="steps-content-slider anim-300">
        <?php

            if(have_rows('elements')):
                while(have_rows('elements')): the_row();
                    $title = get_sub_field('title');
                    $subtitle = get_sub_field('subtitle');
                    $content = get_sub_field('content');
                ?>
                <?php if($content): ?>

                    <div class="content-slide <?php if(get_row_index() == 1) echo 'active' ?>">
                        <h5 class="steps-title"><?=  $title ?></h5>
                        <h6 class="steps-subtitle"><?=  $subtitle ?></h6>
                        <p class="steps-content-txt text-justify"><?=  $content ?></p>
                    </div>

                <?php endif;
                endwhile;
            endif;

        ?>
    </div>



</section>
